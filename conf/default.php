<?php
/**
 * Default settings for the headerbar plugin
 *
 * @author Fernando Cosentino <fbcosentino@yahoo.com.br>
 */

$conf['iconMaxWidth'] = '320';
$conf['iconMaxHeight'] = '240';

