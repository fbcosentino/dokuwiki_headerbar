<?php
/**
 * DokuWiki Plugin headerbar (Syntax Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Fernando Cosentino <fbcosentino@yahoo.com.br>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) {
    die();
}

if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'syntax.php');

class syntax_plugin_headerbar extends DokuWiki_Syntax_Plugin
{
    protected $entry_pattern   = '<hbar\b.*?>(?=.*?</hbar>)';
    protected $exit_pattern    = '</hbar>';

    /**
     * @return string Syntax mode type
     */
    public function getType()
    {
        return 'formatting';
    }

    function getAllowedTypes() { return array('container', 'formatting', 'substition', 'protected', 'disabled', 'paragraphs'); }

    /**
     * @return string Paragraph type
     */
    public function getPType()
    {
        return 'stack';
    }

    /**
     * @return int Sort order - Low numbers go before high numbers
     */
    public function getSort()
    {
        return 195;
    }

    /**
     * Connect lookup pattern to lexer.
     *
     * @param string $mode Parser mode
     */
    public function connectTo($mode)
    {
        $this->Lexer->addEntryPattern($this->entry_pattern,$mode,'plugin_headerbar');
    }

    public function postConnect()
    {
		$this->Lexer->addExitPattern($this->exit_pattern, 'plugin_headerbar');
    }

    /**
     * Handle matches of the headerbar syntax
     *
     * @param string       $match   The match of the syntax
     * @param int          $state   The state of the handler
     * @param int          $pos     The position in the document
     * @param Doku_Handler $handler The handler
     *
     * @return array Data for the renderer
     */
    public function handle($match, $state, $pos, Doku_Handler $handler)
    {
        switch ($state) {
            case DOKU_LEXER_ENTER:
				if (trim($match) == '<hbar>') $data = '';
                else $data = trim(substr($match,5,-1)," \t\n/");
                return array($state, $data);

            case DOKU_LEXER_UNMATCHED:
                $handler->_addCall('cdata', array($match), $pos);
                break;
				

            case DOKU_LEXER_EXIT:
                return array($state, '');

		}
        return false;
    }

    /**
     * Render xhtml output or metadata
     *
     * @param string        $mode     Renderer mode (supported modes: xhtml)
     * @param Doku_Renderer $renderer The renderer
     * @param array         $data     The data from the handler() function
     *
     * @return bool If rendering was successful.
     */
    public function render($mode, Doku_Renderer $renderer, $indata)
    {
        if ($mode !== 'xhtml') {
            return false;
        }
		
        list($state, $data) = $indata;

		switch ($state) {
            case DOKU_LEXER_ENTER:
				$div_color = $this->getConf('color');

				if ($data) { 
					
					$img_params = explode('|',$data,2);
					while (count($img_params) < 2) $img_params[] = '';
					
					$img_data = Doku_Handler_Parse_Media($img_params[0]);
					$img_title = $img_params[1];
					
					global $ID;
					$exists = false;
					$src = $img_data['src'];
					resolve_mediaid(getNS($ID), $src, $exists);		

					if($img_params[0] and $exists)	{
						$gimgs = getImageSize(mediaFN($src));
						$img_ratio = $gimgs[0]/$gimgs[1];
						
						if ($img_data['width']) {
							$imgw = (int)$img_data['width'];
							if ($img_data['height']) {
								$imgh = (int)$img_data['height'];
							}
							else {
								$imgh = (int)($imgw/$img_ratio);
							}
						}
						else {
							$max_w = $this->getConf('iconMaxWidth');
							$max_h = $this->getConf('iconMaxHeight');
							$max_ratio = $max_w / $max_h;
							
							
							if ($img_ratio <= $max_ratio) {
								// tall image, shrink by height
								if ($gimgs[1] > $max_h) {
									$gimgs[1] = $max_h;
									$gimgs[0] = $max_h*$img_ratio;
								}
							}
							else {
								// wide image, shrink by width
								if ($gimgs[0] > $max_w) {
									$gimgs[0] = $max_w;
									$gimgs[1] = $max_w/$img_ratio;
								}
							}
							$imgw = (int)$gimgs[0];
							$imgh = (int)$gimgs[1];
						}
						
						
						$renderer->doc .= '<div class="headerbar_outer" >';
						$renderer->doc .= '<table width="100%" border="0" class="headerbar_table"><tr>
											<td rowspan="2" class="headerbar_img" width="'.($imgw+100).'" align="center">';
						$renderer->internalmedia($src, null, 'center', $imgw, $imgh);
						$renderer->doc .= '</td>';
						$renderer->doc .= '<td class="headerbar_title" width="">'.$img_title.'</td></tr>';
						$renderer->doc .= '<tr><td class="headerbar_inner">';
						
					}
					else {
						// no image
						$renderer->doc .= '<div class="headerbar_outer" >';
						$renderer->doc .= '<table width="100%" border="0" class="headerbar_table"><tr>
											<td rowspan="2" class="headerbar_img" width="'.($imgw+100).'" align="center">';
						$renderer->doc .= '&nbsp;</td>';
						$renderer->doc .= '<td class="headerbar_title" width="">'.$img_title.'</td></tr>';
						$renderer->doc .= '<tr><td class="headerbar_inner">';

					}
				}
				else {
					// If you want a custom blank bar, render it here
					$renderer->doc .= '<div class="headerbar_outer" >';
					$renderer->doc .= '<table width="100%" border="0" class="headerbar_table"><tr>
										<td rowspan="2" class="headerbar_img" width="'.($imgw+100).'" align="center">';
					$renderer->doc .= '&nbsp;</td>';
					$renderer->doc .= '<td class="headerbar_title" width="">&nbsp;</td></tr>';
					$renderer->doc .= '<tr><td class="headerbar_inner">';				
				}

				break;
		
			case DOKU_LEXER_EXIT:
				$renderer->doc .= '</td></tr></table></div>';
				break;

		}
		return true;
    }
}

