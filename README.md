# HeaderBar #

This is a dokuwiki plugin to show a simple title and subtitle text in a bar, alongside with an icon. All items (icon, title, subtitle) are optional.